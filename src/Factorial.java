
public class Factorial {
	static String indent = " ";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 9;
		
		long fact = 1;
		for(int i = 1; i <= n; i++)
		{
			fact = fact * i;
		}
		System.out.println(fact);
		factorialRecursive(9);
	}

	public static int factorialRecursive(int n){
		int temp;
		indent = indent + "  ";
		System.out.println("enter " + indent + n);
		
		if(n ==1 || n == 0)
		{
			System.out.println("return " + indent + 1);
			return 1;
		}else{
			temp = n * factorialRecursive(n - 1);
			indent = indent.substring(2);
			System.out.println("return " + indent + temp);
//			return n * factorialRecursive( n - 1 );
			return temp;
		}
	}
}
