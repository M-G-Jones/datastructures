
public class Fact {

	public static void main(String[] args)
	{
		int n = 9;
		System.out.println("Factorial of 9 " + fact(n));
	}
	public static int fact(int n){
		int temp = 1;
		if ( n == 0) return 1;
		if (n== 1) return 1;
		else
			return n * fact(n - 1);
	}
}
