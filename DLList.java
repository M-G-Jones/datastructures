/**
 * Created by mjones on 4/25/16.
 * /**
 * Created by mjones on 4/25/16.
 * Design, implement, and test a doubly linked list ADT, using DLLNo de objects as
 the nodes. In addition to our standard list operations, your class should provide
 for backward iteration through the list. To support this operation, it should
 export a r e s e t Ba c k method and a g e t Pr e vi ou s method. To facilitate this, you
 may want to include an instance variable l a s t that always references the last
 element on the list.
 */

public class DLList<T> implements ListInterface<T> {

    private int size;
    protected DDLNode<T> last;
    protected DDLNode<T> location;
    protected DDLNode<T> pointer;
    protected DDLNode<T> previous;
    protected boolean found;

    public DLList(){
        this.last = null;
        this.size = 0;
    }

    public void find(T element){
        pointer = last;
        found = false;

        if(last != null){
            do{
                previous = pointer;
                pointer = pointer.getLink();
                if(pointer.getInfo().equals(element))
                    found = true;
            }while((location != last) && !found);


        }
    }



    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void add(T element) {
        DDLNode newNode = new DDLNode<T>(element);
        if(last == null){
            last = newNode;
            newNode.setLink(last);
        } else {
            newNode.setLink(last.getLink());
            last.setLink(newNode);
            last = newNode;
        }
        this.size++;
    }

    @Override
    public boolean remove(T element) {

        find(element);
        if(found){
            if(last == last.getLink()){
                last = null;
            } else {
                if(previous.getLink() == last){
                    last = previous;
                }
                previous.setLink(pointer.getLink());
                size--;
            }
        }
        return found;
    }

    @Override
    public boolean contains(T element) {
        find(element);
        return found;
    }

    @Override
    public T get(T element) {
        find(element);
        if(found){
            return pointer.getInfo();
        } else {
            return null;
        }
    }

    @Override
    public void reset() {
        if(last != null){
            location = last.getLink();
            this.location = last.getLink();
        }
    }

    @Override
    public T getNext() {
        T info = location.getInfo();
        location = location.getLink();
        return info;
    }

    public T getPrevious(){
        T info = location.getInfo();
        location = location.getBack();
        return info;
    }
    @Override
    public String toString(){
        String newLine = System.getProperty("line.separator");
        String list;
        if(this.size() == 0){
           list = "This double-link list is empty...";
        } else {
            reset();
            int i = 1;
            list = "Forward---" + newLine + i + ": " + last.getInfo().toString() + newLine;
            T loop = getPrevious();
            while(loop != null){
                i++;
                list += (i + ": " + location.getInfo().toString() + newLine);
                loop = getPrevious();
            }
            i = 1;
            location = last;
            list += "Backward---";
            loop = location.getInfo();
            while(loop != null){
                list += (i + ": " + this.location.getInfo().toString() + newLine);
                i++;
                loop = this.getPrevious();
            }
            list += (i + ": " + this.location.getInfo().toString() + newLine);
        }

        return list;
    }

}
