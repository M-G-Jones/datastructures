/**
 * Created by mjones on 4/25/16.
 */
public class DDDriver {
    public static void main(String[] args)
    {
        DLList<String> stringList = new DLList<String>();
        stringList.add("String");
        stringList.add("String2");
        stringList.add("String3");
        stringList.add("String4");
        stringList.add("string5");
        System.out.println(stringList.get("String"));
        System.out.println("'String' string removed: " + stringList.remove("String"));
        System.out.println("'String2' string removed: " + stringList.remove("String2"));
        System.out.println("'String4' string removed: " + stringList.remove("String4"));
        System.out.print(stringList);
        System.out.println("Mike");

    }
}
