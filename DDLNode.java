/**
 * Created by mjones on 4/25/16.
 * Design, implement, and test a doubly linked list ADT, using DLLNo de objects as
 the nodes. In addition to our standard list operations, your class should provide
 for backward iteration through the list. To support this operation, it should
 export a r e s e t Ba c k method and a g e t Pr e vi ou s method. To facilitate this, you
 may want to include an instance variable l a s t that always references the last
 element on the list.
 */

public class DDLNode<T> {

    protected DDLNode<T> back;
    protected DDLNode<T> link;
    protected T info;
    public DDLNode(T info) {
        this.info = info;
        link = null;
        back = null;
    }

    public void setBack(DDLNode<T> back){

        this.back =  back;
    }

    public DDLNode<T> getBack(){

        return back;
    }

    public void setLink(DDLNode<T> link)
    // Sets link of this LLTNode.
    {
        this.link = link;
    }

    public DDLNode<T> getLink()
    // Returns link of this LLTNode.
    {
        return link;
    }

    public void setInfo(T info)
    // Sets info T of this LLTNode.
    {
        this.info = info;
    }

    public T getInfo()
    // Returns info T of this LLTNode.
    {
        return info;
    }

    @Override
    public boolean equals(Object node) {
        return this.getInfo() == ((DDLNode<T>)node).getInfo();
    }
}
